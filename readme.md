Cours Application mobile - groupe 8 : Benjamin Molinet - Guillaume Plaideau

Objectif du TP : 
	Data Binding - MVVM
	

A tester : 
	Lecture de la musique -> OK 
	Previous - Pause/Play - Next -> OK
	SeekBar pour avancer dans la musique -> OK
	TODO : SeekBar mise à jour en temps réel - KO
	Notification de l'appli -> OK
	TODO : Quand appuie dessus, renvoie vers l'appli -> KO
	TODO : Ajout play/pause Previous Next sur la notif -> KO

PS: NE PAS LANCER LES MUSIQUES AVEC 00:00 !! CE SONT DES FICHIERS VIDES VENANT DE L'API LAST.FM QUI N'ONT PAS DE BANDE SON, CRASH DE L'APPLICATION, ABSENCE DE LA GESTION DE MANQUE DE SON....... TODO