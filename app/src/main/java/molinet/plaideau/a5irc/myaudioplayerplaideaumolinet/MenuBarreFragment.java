package molinet.plaideau.a5irc.myaudioplayerplaideaumolinet;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import molinet.plaideau.a5irc.myaudioplayerplaideaumolinet.databinding.MenuBarreFragmentBinding;

public class MenuBarreFragment extends Fragment {

    MenuBarreFragmentBinding binding;
    private MyListenerPlayButton myListener;
    private MyListenerPreviousButton myListenerPreviousButton;
    private MyListenerNextButton myListenerNextButton;
    private boolean enLecture = true;

    public void setMyListener(MyListenerPlayButton listener) {
        myListener = listener;
    }

    public void setMyListenerPreviousButton(MyListenerPreviousButton listener) {
        myListenerPreviousButton = listener;
    }

    public void setMyListenerNextButton(MyListenerNextButton listener) {
        myListenerNextButton = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        this.binding = DataBindingUtil.inflate(inflater,
                R.layout.menu_barre_fragment, container, false);

        //Ajout du listener sur le bouton play
        binding.buttonPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPlayClick();
            }
        });

        //Bouton Previous
        binding.buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPreviousClick();
            }
        });

        //Bouton Next
        binding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNextClick();
            }
        });

        return binding.getRoot();
    }

    private void onPlayClick() {
        enLecture = !enLecture;
        changeButtonPlayState();
        myListener.onDoneSomething();
    }

    private void onPreviousClick() {

        myListenerPreviousButton.onDoneSomething(new AudioFile());
        System.out.println("Previous");
    }

    private void onNextClick() {

        myListenerNextButton.onDoneSomething(new AudioFile());
        System.out.println("Next");
    }

    public void changeButtonPlayState() {
        if (enLecture) {
            this.binding.buttonPlayPause.setText(getResources().getString(R.string.pause));
        } else {
            this.binding.buttonPlayPause.setText(getResources().getString(R.string.play));
        }
    }

}