package molinet.plaideau.a5irc.myaudioplayerplaideaumolinet;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import molinet.plaideau.a5irc.myaudioplayerplaideaumolinet.databinding.AudioFileItemBinding;


public class AudioFileListAdapter extends RecyclerView.Adapter<AudioFileListAdapter.ViewHolder> {
    List<AudioFile> audioFileList;
    MyListenerListMusique myListener;

    public AudioFileListAdapter(List<AudioFile> fileList, MyListenerListMusique myListener) {

        assert fileList != null;
        this.audioFileList = fileList;
        assert myListener != null;
        this.myListener = myListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AudioFileItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.audio_file_item, parent, false);

        ViewHolder holder = new ViewHolder(binding);
        holder.viewModel.setMyListener(myListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AudioFile file = audioFileList.get(position);
        holder.viewModel.setAudioFile(file);
    }

    @Override
    public int getItemCount() {
        return audioFileList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private AudioFileItemBinding binding;
        private AudioFileViewModel viewModel = new AudioFileViewModel();

        ViewHolder(AudioFileItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setAudioFileViewModel(viewModel);
        }
    }
}
