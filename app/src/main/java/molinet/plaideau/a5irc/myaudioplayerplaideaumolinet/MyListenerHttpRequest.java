package molinet.plaideau.a5irc.myaudioplayerplaideaumolinet;

public interface MyListenerHttpRequest {
    String onHttpRequestCompleted(String result);
}
