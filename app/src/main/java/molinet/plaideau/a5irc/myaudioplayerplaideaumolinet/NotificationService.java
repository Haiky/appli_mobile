package molinet.plaideau.a5irc.myaudioplayerplaideaumolinet;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationService {

    public Notification n;
    public Context context;
    private MyListenerPlayButtonNotification myListener;
    private MyListenerPreviousButtonNotification myListenerPreviousButton;
    private MyListenerNextButtonNotification myListenerNextButton;
    private NotificationManager notificationManager;

    public NotificationService(Context context) {
        this.context = context;
    }

    public void setMyListener(MyListenerPlayButtonNotification listener) {
        myListener = listener;
    }

    public void setMyListenerPreviousButton(MyListenerPreviousButtonNotification listener) {
        myListenerPreviousButton = listener;
    }

    public void setMyListenerNextButton(MyListenerNextButtonNotification listener) {
        myListenerNextButton = listener;
    }

    public void setNotification(AudioFile audioFile) {

//            Intent intent = new Intent(context, MainActivity.class);
//            PendingIntent pIntent = PendingIntent.getActivity(context, 0 , intent, 0);

        n = new Notification.Builder(context)
                .setContentTitle(audioFile.getTitle())
                .setContentText(audioFile.getArtist())
                .setSmallIcon(R.drawable.ic_launcher_foreground)
//                    .setContentIntent(pIntent)
//                    .addAction(R.drawable.ic_launcher_foreground,"Application",pIntent)
                .build();
        notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, n);
    }

//        @Override
//        public void onReceive(Context context, Intent intent) {
//            System.out.println("coucou c'est moi");
//            myListener.onDoneSomething(1);
//        }
}
