package molinet.plaideau.a5irc.myaudioplayerplaideaumolinet;

import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import molinet.plaideau.a5irc.myaudioplayerplaideaumolinet.databinding.AudioFileListFragmentBinding;

public class AudioFileListFragment extends Fragment {

    AudioFileListFragmentBinding binding;
    Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
    private MyListenerListMusique myListener;
    private List<AudioFile> listMusique = new ArrayList<>();

    public void setMyListener(MyListenerListMusique listener) {
        myListener = listener;
    }


    public void setListMusique(List<AudioFile> musiques) {
        listMusique.addAll(musiques);
        binding.audioFileList.setAdapter(new AudioFileListAdapter(listMusique, myListener));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,
                R.layout.audio_file_list_fragment, container, false);
        // Permet de définir le Layout manager et sa forme
        binding.audioFileList.setLayoutManager(new LinearLayoutManager(binding.getRoot().getContext()));

        String[] projection = {
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.DURATION,
        };
        Cursor cursor = getContext().getContentResolver().query(uri, projection, null, null, null);

        int id = 0;
        while (cursor.moveToNext()) {
            final String title = cursor.getString(1);
            final String artist = cursor.getString(2);
            final String album = cursor.getString(3);
            final String filePath = cursor.getString(0);
            final int duration = Integer.parseInt(cursor.getString(4)) / 1000;
            listMusique.add(new AudioFile(title, filePath, artist, album, "Hard Rock", 1990, duration, id));
            id++;
        }
        binding.audioFileList.setAdapter(new AudioFileListAdapter(listMusique, myListener));
        return binding.getRoot();
    }

    public List<AudioFile> getMusique() {
        return listMusique;
    }
}