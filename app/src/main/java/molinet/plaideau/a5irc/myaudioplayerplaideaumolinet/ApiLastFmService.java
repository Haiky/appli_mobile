package molinet.plaideau.a5irc.myaudioplayerplaideaumolinet;

public class ApiLastFmService {

    /*URL avec ma clé API :
    http://ws.audioscrobbler.com/2.0/?method=track.search&track=Believe&api_key=b4a215c9dfe95a2c9649eef7fab6ce54&format=json
    */

    private MyListenerHttpRequest myListenerHttpRequest;

    public void setMyListenerHttpRequest(MyListenerHttpRequest myListenerHttpRequest) {
        this.myListenerHttpRequest = myListenerHttpRequest;
    }


    public void getTrack() {
        HttpRequestService httpRequestService = new HttpRequestService();
        httpRequestService.setMyListenerHttpRequest(new MyListenerHttpRequest() {
            @Override
            public String onHttpRequestCompleted(String result) {
                myListenerHttpRequest.onHttpRequestCompleted(result);
                return result;
            }
        });

        httpRequestService.execute("http://ws.audioscrobbler.com/2.0/?method=track.search&track=a&api_key=b4a215c9dfe95a2c9649eef7fab6ce54&format=json");
    }
}
