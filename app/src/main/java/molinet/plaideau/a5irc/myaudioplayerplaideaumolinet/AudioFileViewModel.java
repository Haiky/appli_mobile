package molinet.plaideau.a5irc.myaudioplayerplaideaumolinet;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

public class AudioFileViewModel extends BaseObservable {
    private AudioFile audioFile = new AudioFile();
    private MyListenerListMusique myListener;

    public void setAudioFile(AudioFile file) {
        audioFile = file;
        notifyChange();
    }

    @Bindable
    public String getArtist() {
        return audioFile.getArtist();
    }

    @Bindable
    public String getTitle() {
        return audioFile.getTitle();
    }

    @Bindable
    public String getAlbum() {
        return audioFile.getAlbum();
    }

    @Bindable
    public String getDuration() {
        return audioFile.getDurationText();
    }

    public void onTitleClick() {
        System.out.println("listened");
        myListener.onDoneSomething(audioFile);
    }

    public void setMyListener(MyListenerListMusique myListener) {
        this.myListener = myListener;
    }
}
