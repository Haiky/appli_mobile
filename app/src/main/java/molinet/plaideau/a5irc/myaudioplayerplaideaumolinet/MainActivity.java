package molinet.plaideau.a5irc.myaudioplayerplaideaumolinet;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import molinet.plaideau.a5irc.myaudioplayerplaideaumolinet.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    static final int MY_PERMISSIONS_REQUEST_READ_MEDIA = 0;
    final MusicPlayedFragment musicFragment = new MusicPlayedFragment();
    private ActivityMainBinding binding;
    private boolean enLecture;
    private AudioFile musiqueEnCours;
    private List<AudioFile> listMusique;
    private Context context;
    private Thread threadMAJTempsEcoute;
    private MusicPlayerService musicPlayerService = new MusicPlayerService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        Intent intentPlayer = new Intent(getApplicationContext(), MusicPlayerService.class);
        ServiceConnection serviceConnectionPlayer = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder binder) {
                Log.d("ServiceConnection", "connected");
                musicPlayerService = ((MusicPlayerService.PlayerBinder) binder).getService();
            }
            //binder comes from server to communicate with method's of

            public void onServiceDisconnected(ComponentName className) {
                Log.d("ServiceConnection", "disconnected");
                musicPlayerService = null;
            }
        };

        bindService(intentPlayer, serviceConnectionPlayer, Context.BIND_AUTO_CREATE);

        //Permission SD card
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_MEDIA);
        }

        //Thread permettant de mettre à jour le chrono à gauche de la barre d'avancement de la musique
        threadMAJTempsEcoute = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(100);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                musicFragment.updateStartDuration(musicPlayerService.getDuration());
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        showStartup();
    }

    public void showStartup() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        final AudioFileListFragment audioFragment = new AudioFileListFragment();
        final MenuBarreFragment barreFragment = new MenuBarreFragment();
        final NotificationService notificationService = new NotificationService(context);

        //Ajout musique internet
        ApiLastFmService apiLastFmService = new ApiLastFmService();
        apiLastFmService.setMyListenerHttpRequest(new MyListenerHttpRequest() {
            @Override
            public String onHttpRequestCompleted(String result) {
                List<AudioFile> listMusique = new ArrayList<>();
                try {
                    JSONObject json = new JSONObject(result);
                    json = json.getJSONObject("results");
                    json = json.getJSONObject("trackmatches");

                    JSONArray tracks = json.getJSONArray("track");
                    for (int i = 0; i < json.length(); i++) {


                        json = tracks.getJSONObject(i);
                        String name = json.getString("name");
                        String artist = json.getString("artist");
                        String url = json.getString("url");
                        String image = json.getString("image");


                        AudioFile musique = new AudioFile();
                        musique.setTitle(name);
                        musique.setArtist(artist);
                        listMusique.add(musique);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                audioFragment.setListMusique(listMusique);
                return "";
            }
        });
        apiLastFmService.getTrack();


        //LISTE DES MUSIQUES
        audioFragment.setMyListener(new MyListenerListMusique() {
            @Override
            public void onDoneSomething(AudioFile audioFile) {
                //Pour afficher le nom et la durée de la musique
                listMusique = audioFragment.getMusique();
                musicFragment.setAudioFile(audioFile);
                try {
                    musicPlayerService.stop();
                    musicPlayerService.play(audioFile.getFilePath());
                    notificationService.setNotification(audioFile);
                    musiqueEnCours = audioFile;
                    barreFragment.changeButtonPlayState();
                    threadMAJTempsEcoute.start();
                    enLecture = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        //Notification
        notificationService.setMyListener(new MyListenerPlayButtonNotification() {
            @Override
            public int onDoneSomething(int i) {
                if (enLecture) {
                    threadMAJTempsEcoute.interrupt();
                    musicFragment.updateStartDuration(musicPlayerService.getDuration());
                    musicPlayerService.pause();
                    enLecture = !enLecture;
                } else {
                    musicPlayerService.play();
                    threadMAJTempsEcoute.start();
                    enLecture = !enLecture;
                }
                return i;
            }
        });




        //BARRE MENU
        barreFragment.setMyListener(new MyListenerPlayButton() {
            @Override
            public void onDoneSomething() {
                if (enLecture) {
                    threadMAJTempsEcoute.interrupt();
                    musicFragment.updateStartDuration(musicPlayerService.getDuration());
                    musicPlayerService.pause();
                    enLecture = !enLecture;
                } else {
                    musicPlayerService.play();
                    threadMAJTempsEcoute.start();
                    enLecture = !enLecture;
                }
            }
        });

        barreFragment.setMyListenerPreviousButton(new MyListenerPreviousButton() {
            @Override
            public void onDoneSomething(AudioFile audioFile) {
                musiqueEnCours = listMusique.get(musiqueEnCours.getId() - 1);
                try {
                    musicPlayerService.stop();
                    musicPlayerService.play(musiqueEnCours.getFilePath());
                    musicFragment.setAudioFile(musiqueEnCours);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        barreFragment.setMyListenerNextButton(new MyListenerNextButton() {
            @Override
            public void onDoneSomething(AudioFile audioFile) {
                musiqueEnCours = listMusique.get(musiqueEnCours.getId() + 1);
                try {
                    musicPlayerService.stop();
                    musicPlayerService.play(musiqueEnCours.getFilePath());
                    musicFragment.setAudioFile(musiqueEnCours);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        musicFragment.setMyListenerSeekBar(new MyListenerSeekBar() {
            @Override
            public void onDoneSomething(int time) {
                musicPlayerService.forwardSong(time);
            }
        });


        transaction.replace(R.id.fragment_container, audioFragment);
        transaction.replace(R.id.fragment_music_played, musicFragment);
        transaction.replace(R.id.fragment_menu, barreFragment);
        transaction.commit();
    }
}
