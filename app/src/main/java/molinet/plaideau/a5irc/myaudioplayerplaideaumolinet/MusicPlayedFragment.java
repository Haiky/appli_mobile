package molinet.plaideau.a5irc.myaudioplayerplaideaumolinet;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import java.util.Locale;

import molinet.plaideau.a5irc.myaudioplayerplaideaumolinet.databinding.MusicPlayedFragmentBinding;


public class MusicPlayedFragment extends Fragment {

    public AudioFile audioFile;
    private MyListenerSeekBar myListenerSeekBar;
    private MusicPlayedFragmentBinding binding;

    public void setMyListenerSeekBar(MyListenerSeekBar myListenerSeekBar) {
        this.myListenerSeekBar = myListenerSeekBar;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,
                R.layout.music_played_fragment, container, false);


        /*
        Binding avec l'affichage et la barre d'avancement
        onProgressChanged: mis à jour la durée + contact avec un listener pour forward la musique
         */
        binding.duration.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String time;
                int second = progress % 60;
                int durationMinute = (progress - second) / 60;
                int minute = durationMinute % 60;
                int hour = (durationMinute - minute) / 60;
                if (hour > 0)
                    time = String.format(Locale.getDefault(),
                            "%02d:%02d:%02d", hour, minute, second);
                time = String.format(Locale.getDefault(),
                        "%02d:%02d", minute, second);
                binding.startDuration.setText(time);
                //Forward song avec la durée voulue

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //On ne fait rien au début du contact de l'utilisateur sur la barre d'avancement
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                myListenerSeekBar.onDoneSomething(progress);            }
        });
        return binding.getRoot();
    }


    /*
    Fonction d'affichage d'information sur la musique :
    Titre + temps avancement musique + barre d'avancement + durée
    */

    public void setAudioFile(AudioFile audioFile) {
        if (!audioFile.getTitle().isEmpty())
            this.audioFile = audioFile;
        binding.musicPlayed.setText(audioFile.getTitle());
        binding.startDuration.setText("00:00");
        binding.totalDuration.setText(audioFile.getDurationText());
        binding.duration.setMax(audioFile.getDuration());
    }

    /*
    Fonction de mise à jour de la durée dans l'affichage,
    à droite de la barre d'avancement de la musique
     */

    public void updateStartDuration(int progress) {
        String time;
        int second = progress % 60;
        int durationMinute = (progress - second) / 60;
        int minute = durationMinute % 60;
        int hour = (durationMinute - minute) / 60;
        if (hour > 0)
            time = String.format(Locale.getDefault(),
                    "%02d:%02d:%02d", hour, minute, second);
        time = String.format(Locale.getDefault(),
                "%02d:%02d", minute, second);
        binding.startDuration.setText(time);
        binding.duration.setProgress(progress);
    }
}
